#!/bin/bash

BUILD="${1:-wb45n_devel}"
if [ -z $2 ]
then
	DEST="/tftpboot"
else
	DEST="/tftpboot/$2"
	if [ ! -e $DEST ]
	then
		mkdir -p $DEST
	fi
fi

PREFIX="."

cp -v $PREFIX/output/$BUILD/images/at91bs.bin $DEST/bootstrap.bin
cp -v $PREFIX/output/$BUILD/images/at91bs.bin $DEST/at91bs.bin
cp -v $PREFIX/output/$BUILD/images/u-boot.bin $DEST/.
cp -v $PREFIX/output/$BUILD/images/uImage.wb??n $DEST/kernel.bin || \
  cp -v $PREFIX/output/$BUILD/images/uImage.wb??n_* $DEST/kernel.bin || \
  cp -v $PREFIX/output/$BUILD/images/uImage $DEST/kernel.bin
cp -v $PREFIX/output/$BUILD/images/rootfs.ubi $DEST/rootfs.bin
cp -v $PREFIX/output/$BUILD/images/fw.txt $DEST/fw.txt
