#!/usr/bin/kermit +
set modem type none
set line /dev/ttyACM\%1
set carrier-watch off
set speed 115200

set handshake none
set flow-control none
robust
set file type bin
set file name lit
set rec pack 1000
set send pack 1000
set window 5

set terminal cr-display crlf

set key \127 \8
connect
