#!/bin/bash
[ $# -gt 0 ] &&
exec gdb "$1" --return-child-result --silent --batch \
        --ex "run $([ $# -gt 1 ] && printf '%q ' "${@:2}")" \
        --ex bt \
        2>/dev/null
#
