#!/bin/bash
rev=$1
if [[ -z $rev ]] ; then
    echo "[rev] is required."
    exit
fi
# create history branch from $rev
git branch history $rev
parent_rev=`git rev-parse $rev~1`
combined_rev=`echo '[Truncated history]' | git commit-tree "$parent_rev^{tree}"`
git rebase --onto $combined_rev $parent_rev
echo "Now you can push history branch to remote."
