#!/bin/bash

# default options assume supersniffer
SNIFFER=supersniffer
CHANNEL=6
MAC="00:1d:e0:27:1a:29"
IFACE=""

function usage {

	echo "Launch a sniffer"
	echo "Usage:"
	echo "$0 [options]"
	echo
	echo "    -h              print this message"
	echo
	echo "    -s <sniffer>    hostname of sniffer (default is $SNIFFER)"
	echo
	echo "    -c <channel>    Channel to sniff (default is $CHANNEL)"
	echo
	echo "    -i <interface>  Sniff on this interface.  If this is supplied, it"
	echo "                    overrides the -m option.  (default is not set)"
	echo
	echo "    -m <macaddr>    Sniff on the iface with this mac (default: $MAC)"
}

while getopts "hs:c:i:m:" opt; do
	case "$opt" in
		h)
			usage
			exit 0
			;;
		s)
			SNIFFER=$OPTARG
			;;
		c)
			CHANNEL=$OPTARG
			;;
		i)
			IFACE=$OPTARG
			;;
		m)
			MAC=$OPTARG
			;;
		\?)
			echo "Unknown option -$OPTARG"
			exit 1
			;;
	esac
done

MYNAME=`hostname`

if [ "$SNIFFER" = "$MYNAME" ]; then
	if [ ! -e /sys/class/net/rtap0 ]; then
		if [ "$IFACE" = "" ]; then
			for iface in `ls /sys/class/net/ | grep wlan`; do
				if [ "`cat /sys/class/net/$iface/address`" == "$MAC" ]; then
					IFACE=$iface
					break
				fi
			done
		fi
		if [ "$IFACE" = "" ]; then
			echo "Failed to find suitable sniffer interface!"
			exit 1
		fi
		sudo iw dev $IFACE interface add rtap0 type monitor
		sudo ifconfig rtap0 up
	fi
	sudo iwconfig rtap0 channel $CHANNEL
	iwlist rtap0 channel
	sudo wireshark -i rtap0 -k
else
	ssh -X $SNIFFER.local sniff.sh $*
fi
